﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the player's states
/// </summary>
public class StateScript : MonoBehaviour
{
    public enum PlayerState
    {
        TakeOff,
        Moving,
        Boost,
        SlowDown,
        KnockBack,
        Stop
    };

    public PlayerState currentState;
    
    [Header("Scripts")]
    public OrbitAroundBackground playerSpeed;
    public OrbitAroundBackground cameraSpeed;
    public PlayerMovement playerMovement;

    [Header("Timers")]
    public float takeOffTimer;
    public float knockBackTimer;
    public float knockBackTimeAmount;
    public float stopTimer;
    public float stopTimeAmount;
    
    [Header("Bools")]
    public bool slowDownBool;
    public bool knockBackBool;
    public bool stopBool;

    void Start()
    {
        takeOffTimer = 0;
        currentState = PlayerState.TakeOff;

        slowDownBool = false;
        knockBackBool = false;
        stopBool = false;
    }

    void Update()
    {
        takeOffTimer += Time.deltaTime;
        switch (currentState)
        {
            case (PlayerState.TakeOff):
                playerSpeed.currentSpeed = playerSpeed.initialSpeed;
                cameraSpeed.currentSpeed = cameraSpeed.initialSpeed;
                playerMovement.movementLocked = false;
                break;
            case (PlayerState.Moving):
                playerSpeed.currentSpeed = playerSpeed.normalSpeed;
                cameraSpeed.currentSpeed = cameraSpeed.normalSpeed;
                playerMovement.movementLocked = false;
                break;
            case (PlayerState.Boost):
                playerSpeed.currentSpeed = playerSpeed.boostSpeed;
                cameraSpeed.currentSpeed = cameraSpeed.boostSpeed;
                playerMovement.movementLocked = false;
                break;
            case (PlayerState.SlowDown):
                playerSpeed.currentSpeed = playerSpeed.minSpeed;
                cameraSpeed.currentSpeed = cameraSpeed.minSpeed;
                playerMovement.movementLocked = false;
                break;
            case (PlayerState.KnockBack):
                playerSpeed.currentSpeed = playerSpeed.knockBackSpeed;
                cameraSpeed.currentSpeed = cameraSpeed.knockBackSpeed;
                playerMovement.movementLocked = true;
                break;
            case (PlayerState.Stop):
                playerSpeed.currentSpeed = 0;
                cameraSpeed.currentSpeed = 0;
                playerMovement.movementLocked = true;
                break;
        }

        if (takeOffTimer >= 3 && currentState == PlayerState.TakeOff)
        {
            takeOffTimer = 3;
            currentState = PlayerState.Moving;
        }
        else if (Input.GetButton("Jump"))
            currentState = PlayerState.Boost;
        else if (slowDownBool == true)
            currentState = PlayerState.SlowDown;
        else if (knockBackBool == true)
        {
            currentState = PlayerState.KnockBack;
            knockBackTimer += Time.deltaTime;
            if (knockBackTimer >= knockBackTimeAmount)
            {
                currentState = PlayerState.Moving;
                knockBackTimer = 0;
                knockBackBool = false;
            }
        }
        else if (stopBool == true)
        {
            currentState = PlayerState.Stop;
            stopTimer += Time.deltaTime;
            if(stopTimer >= stopTimeAmount)
            {
                currentState = PlayerState.Moving;
                stopTimer = 0;
                stopBool = false;
            }
        }
        else
            currentState = PlayerState.Moving;
    }
}
