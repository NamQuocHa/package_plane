﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Stores barn count, required package and remaining package
/// </summary>
public class BarnScript : MonoBehaviour
{
    [Header("Barn")]
    public float barnTimer;
    public float barnTimeAmount;
    public bool barnRest;

    [Header("Packages")]
    public float requiredPackage;
    public CollectingScript collectingScript;

	// Use this for initialization
	void Start ()
    {
        barnRest = false;
        requiredPackage = Random.Range(1, 6);
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(barnRest == true)
        {
            gameObject.tag = "BarnDisable";
            requiredPackage = 0;
            barnTimer += Time.deltaTime;
            if(barnTimer >= barnTimeAmount)
            {
                requiredPackage = Random.Range(1, 6);
                gameObject.tag = "Barn";
                barnRest = false;
                barnTimer = 0;
            }
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("Player went through");
            if (collectingScript.packageCount >= requiredPackage && barnRest == false)
            {
                barnRest = true;
                collectingScript.barnRequired -= 1;
                collectingScript.packageCount -= requiredPackage;
            }
        }
    }
}
