﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Move player up and down
/// </summary>
[System.Serializable]
public class Boundaries
{
    public float yMin, yMax;
}

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody rb;
    public float verticalSpeed = 5f;
    public Boundaries boundary;

    public bool movementLocked;

    private float VInput;

	void Start ()
    {
        rb = gameObject.GetComponent<Rigidbody>();
	}

    void FixedUpdate()
    {
        VInput = Input.GetAxis("Vertical");

        if (movementLocked == false)
        {
            Vector3 VPosition = transform.position + transform.up * VInput * verticalSpeed * Time.deltaTime;
            rb.MovePosition(VPosition);
        }

        Vector3 clampedPosition = transform.position;
        clampedPosition.y = Mathf.Clamp(transform.position.y, boundary.yMin, boundary.yMax);
        transform.position = clampedPosition;
    }
}
