﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Orbit around an object as its center
/// Incorporated by LeMoine found here: http://answers.unity3d.com/questions/463704/smooth-orbit-round-object-with-adjustable-orbit-ra.html
/// </summary>
public class OrbitAroundBackground : MonoBehaviour
{
    GameObject player;

    [Header("OrbitAroundObject")]
    public Transform center;
    public Vector3 axis = Vector3.up;
    public Vector3 desiredPosition;
    public float radius = 2.0f;

    [Header("Speed")]
    public float currentSpeed;
    public float normalSpeed;
    public float minSpeed;
    public float boostSpeed;
    public float initialSpeed;
    public float knockBackSpeed;

    [Header("Speed Modifier")]
    public float minSpeedModifier;
    public float boostSpeedModifier;
    public float initialSpeedModifier;
    public float knockBackSpeedModifier;


    void Start()
    {
        currentSpeed = normalSpeed;

        minSpeed = normalSpeed * minSpeedModifier;
        boostSpeed = normalSpeed * boostSpeedModifier;
        initialSpeed = normalSpeed * initialSpeedModifier;
        knockBackSpeed = normalSpeed * knockBackSpeedModifier;

        center = player.transform;
        transform.position = (transform.position - center.position).normalized * radius + center.position;
    }

    void Update()
    {
        transform.RotateAround(center.position, axis, currentSpeed * Time.deltaTime);
        desiredPosition = (transform.position - center.position).normalized * radius + center.position;
    }
}
