﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Manages transition between scenes
/// </summary>
public class SceneTransition : MonoBehaviour
{
    public void LoadLevel(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }
}
