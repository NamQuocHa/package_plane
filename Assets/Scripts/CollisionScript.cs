﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages collision with player
/// </summary>
public class CollisionScript : MonoBehaviour
{
    public StateScript stateScript;
    public CollectingScript collectScript;

    void OnCollisionEnter (Collision other)
    {
        if(other.gameObject.CompareTag("Knockback"))
        {
            stateScript.knockBackBool = true;
        }
        else if (other.gameObject.CompareTag("Bird"))
        {
            stateScript.stopBool = true;
        }
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.CompareTag("Package"))
        {
            collectScript.packageCount += 1;
        }
    }

    void OnTriggerStay (Collider other)
    {
        if (other.gameObject.CompareTag("Slow"))
        {
            stateScript.slowDownBool = true;
        }
    }

    void OnTriggerExit (Collider other)
    {
        if(other.gameObject.CompareTag("Slow"))
        {
            stateScript.slowDownBool = false;
        }
    }
}
