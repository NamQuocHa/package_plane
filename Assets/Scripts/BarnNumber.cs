﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Displays the barn number when player is near a barn
/// </summary>
public class BarnNumber : MonoBehaviour
{
    [Header("Barn Script")]
    public BarnScript barnScript;

    [Header("Renderer")]
    public MeshRenderer meshRender;

    [Header("Text")]
    public TextMesh barnText;

    // Use this for initialization
    void Start ()
    {
        meshRender.enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        barnText.text = barnScript.requiredPackage.ToString();
    }
    
    void OnTriggerStay(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            meshRender.enabled = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            meshRender.enabled = false;
        }
    }
}
