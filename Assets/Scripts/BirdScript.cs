﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Scripts to make the bird work
/// Bird will have knockback and movement
/// </summary>
public class BirdScript : MonoBehaviour
{
    [Header("Knockback")]
    public float knockBackAmount;
    public float knockBackTime;
    public float knockBackDuration;
    public bool knockBackBool;

    [Header("Movement")]
    public float currentSpeed;
    public float birdSpeed;
    
    [Header("Orbit")]
    public GameObject birdObject;
    public Transform center;
    public Vector3 axis = Vector3.up;
    public Vector3 desiredPosition;
    public float radius = 2.0f;

    // Use this for initialization
    void Start ()
    {
        knockBackBool = false;
        currentSpeed = birdSpeed;

        //birdObject = GameObject.FindGameObjectWithTag("Background");
        center = birdObject.transform;
        transform.position = (transform.position - center.position).normalized * radius + center.position;
    }
	
	// Update is called once per frame
	void Update ()
    {
        transform.RotateAround(center.position, axis, currentSpeed * Time.deltaTime);
        desiredPosition = (transform.position - center.position).normalized * radius + center.position;

        if (knockBackBool == true)
        {
            knockBackTime += Time.deltaTime;
            currentSpeed = knockBackAmount;
            if (knockBackTime >= knockBackDuration)
            {
                currentSpeed = birdSpeed;
                knockBackTime = 0;
                knockBackBool = false;
            }
        }
    }

    void OnCollisionEnter (Collision other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            Debug.Log("Player Collision");
            knockBackBool = true;
        }
    }
}
