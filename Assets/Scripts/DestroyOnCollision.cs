﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Destroy Object onplayer collision
/// </summary>
public class DestroyOnCollision : MonoBehaviour
{
    public GameObject packageObject;

    void Start()
    {
        packageObject = gameObject.GetComponent<GameObject>();
    }

    void Destroy()
    {
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            Destroy();
        }
    }
}
