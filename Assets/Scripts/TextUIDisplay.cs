﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Displays all text, except for barn text
/// </summary>
public class TextUIDisplay : MonoBehaviour
{
    [Header("Timer")]
    public float levelTimer;

    [Header("Text")]
    public UnityEngine.UI.Text levelTimeText;
    public UnityEngine.UI.Text barnRequired;
    public TextMesh packageCount;

    [Header("Scripts")]
    public CollectingScript collectingScript;
	
	// Update is called once per frame
	void Update ()
    {
        levelTimer += Time.deltaTime;

        barnRequired.text = "Barn Required: " + collectingScript.barnRequired.ToString();
        packageCount.text = collectingScript.packageCount.ToString();
        levelTimeText.text = "Time: " + levelTimer.ToString("F2");

    }
}
