﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// stores how much package the player has
/// stores how much barns the player will need to clear
/// Go to next scene once barn required reaches zero
/// </summary>
public class CollectingScript : MonoBehaviour
{
    [Header("Package")]
    public float packageCount;

    [Header("Barns Required")]
    public float barnRequired;

    [Header("Scene Transition")]
    public SceneTransition sceneTransition;
    public string SceneName;

    void Start ()
    {
        packageCount = 0;
	}
	
	void Update ()
    {
		if(packageCount >= 8)
        {
            packageCount = 8;
        }

        if(barnRequired <= 0)
        {
            sceneTransition.LoadLevel(SceneName);
        }
	}
}
