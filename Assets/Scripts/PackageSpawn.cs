﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Spawns packages at spawn points
/// For now, each spawn point will have it's own timer to spawn packages
/// </summary>
public class PackageSpawn : MonoBehaviour
{
    public float spawnTimer;
    public float spawnTimeAmount = 16f;
    public GameObject packageObject;
    public Transform spawnTransform;

    void Start()
    {
        spawnTransform = gameObject.GetComponent<Transform>();

        spawnTimer = spawnTimeAmount;
    }

    void Update ()
    {
        spawnTimer += Time.deltaTime;
        if(spawnTimer >= spawnTimeAmount)
        {
            Instantiate(packageObject, spawnTransform);
            spawnTimer -= spawnTimeAmount;
        }
	}
}
